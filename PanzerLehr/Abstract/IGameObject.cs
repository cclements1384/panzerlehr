﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace PanzerLehr.Abstract
{
    public interface IGameObject
    {
        void Load(ContentManager content, GraphicsDevice graphics);
        void Draw(SpriteBatch spriteBatch);
    }
}

﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PanzerLehr.Core;
using PanzerLehr.GameObjects;
using System;

namespace PanzerLehr
{
    public class Game1 : Game
    {
        enum GameState : int
        {
            SplashScreen = 0,
            Running = 1,
            Menu = 2
        }

        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        //The Player Tank
        Vector2 tankPosition;
        Vector2 tankSpeed = new Vector2(50.5f, 50.0f);

        //Building
        GameObject building;
        Vector2 buildingPostion;

        //Splash Screen
        GameObject splashScreen;

        //Splash Screen
        Texture2D spashScreen;

        //opponent
        GameObject panzer4;


        //double speed
        double speed;
        const int MAX_SPEED = 100;

        Rectangle tankBounds;
        Rectangle boundingBlock; //using a transform matrix;

        //Textures
        Color[] tankCollisionData;
        Color[] buildingCollisionData;
        Color[] panzerColorData;

        Texture2D tankTexture;
        Texture2D grassTexture;

        // Screen Parameters
        int screenWidth, screenHeigth;

        float t34rotationAngle;
        Vector2 origin;
        Vector2 buildingOrigin;

        SpriteFont spFont;

        SoundEffect tankgunEffect;
        SoundEffectInstance tankgunInstance;

        SoundEffect tankEngineEffect;
        SoundEffectInstance tankengineInstance;


        Song musicEffect;
        SoundEffectInstance musicInstance;

        GameState gameState;

        bool showDebug = false;

        public Game1()
        {
            this.
            _graphics = new GraphicsDeviceManager(this);
            _graphics.IsFullScreen = false;
            _graphics.PreferredBackBufferHeight = 600;
            _graphics.PreferredBackBufferWidth = 800;
            Content.RootDirectory = "Content";
            gameState = GameState.SplashScreen;
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            // Set my screen parameters; 
            screenWidth = _graphics.GraphicsDevice.Viewport.Width;
            screenHeigth = _graphics.GraphicsDevice.Viewport.Height;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            switch (gameState)
            {
                case GameState.SplashScreen:
                    {
                        LoadSplashScreen();
                        MediaPlayer.Play(musicEffect);
                    }
                    break;
                case GameState.Running:
                    {
                        MediaPlayer.Stop();
                        LoadGameContent();
                    }
                    break;
                case GameState.Menu:
                    {
                        LoadMenuContent();
                    }
                    break;
            }


        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.F4))
            {
                showDebug = !showDebug;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
            if (Keyboard.GetState().IsKeyDown(Keys.X))
            {
                MediaPlayer.Play(musicEffect);
                gameState = GameState.SplashScreen;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                speed += -1;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                if (speed < MAX_SPEED)
                {
                    if (tankengineInstance.State == SoundState.Stopped)
                    {

                        tankengineInstance.Play();
                    }
                    else
                        tankengineInstance.Resume();
                    speed += 1;
                }
            }
            else if (speed > 0)
            {
                if (tankengineInstance.State == SoundState.Playing)
                {

                    tankengineInstance.Stop();
                }
                speed -= 1;
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D))
                t34rotationAngle += .02f;

            if (Keyboard.GetState().IsKeyDown(Keys.A))
                t34rotationAngle -= .02f;

            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (gameState == GameState.SplashScreen)
                {
                    gameState = GameState.Running;
                    LoadContent();
                }

                else
                {
                    if (tankgunInstance.State == SoundState.Stopped)
                        tankgunInstance.Play();
                    else
                        tankgunInstance.Resume();
                }
            }

            float elasped = (float)gameTime.ElapsedGameTime
                    .TotalSeconds;


            switch (gameState)
            {
                case GameState.SplashScreen:
                    {
                        LoadSplashScreen();
                    }
                    break;
                case GameState.Running:
                    {

                        /* update the building */
                        Matrix buildingTransform =
                         Matrix.CreateTranslation(new Vector3(building.Postion, 0.0f));

                        /* update enemy tank */
                        Matrix panzerTransform =
                            Matrix.CreateTranslation(new Vector3(panzer4.Postion, 0.0f));


                        /* update tank position with a transform */
                        Matrix tankTransform =
                                Matrix.CreateTranslation(new Vector3(-origin, 0.0f)) *
                                // Matrix.CreateScale(tank.Scale) *  would go here
                                Matrix.CreateRotationZ(t34rotationAngle) *
                                Matrix.CreateTranslation(new Vector3(tankPosition, 0.0f));

                        /* calculate the bounding block of this texture in worldspace */
                        tankBounds = CollisionEngine.CalculateBoundingRectangle(
                            new Rectangle(0, 0, tankTexture.Width, tankTexture.Height),
                            tankTransform);

                        /* calculate the bounds of this building in world space */
                        building.Bounds =
                          new Rectangle((int)building.Postion.X, (int)building.Postion.Y,
                          building.Texture.Width, building.Texture.Height);

                        /* calculate the bounding block of this texture in worldspace */
                        var panzerBounds = CollisionEngine.CalculateBoundingRectangle(
                            new Rectangle((int)panzer4.Postion.X, (int)panzer4.Postion.Y,
                                panzer4.Texture.Width, panzer4.Texture.Height),
                            tankTransform);

                        /* rotate the tank */
                        tankPosition.X += (float)Math.Cos(t34rotationAngle) * (float)speed * (float)elasped;
                        tankPosition.Y += (float)Math.Sin(t34rotationAngle) * (float)speed * (float)elasped;
                        float circle = MathHelper.Pi * 2;
                        t34rotationAngle = t34rotationAngle % circle;

                        /* first check to see if the boxes intersect. 
                        * if so, perform the pixel test */
                        if (tankBounds.Intersects(building.Bounds))
                            if (CollisionEngine.IntersectPixels(tankTransform, tankTexture.Width,
                                                tankTexture.Height, tankCollisionData,
                                                buildingTransform, building.Texture.Width,
                                                building.Texture.Height, buildingCollisionData))
                                speed = 0;

                        /* check player against enemy tank */
                        if (tankBounds.Intersects(panzer4.Bounds))
                            if (CollisionEngine.IntersectPixels(tankTransform, tankTexture.Width,
                                                tankTexture.Height, tankCollisionData,
                                                panzerTransform, panzer4.Texture.Width,
                                                panzer4.Texture.Height, panzerColorData))
                                speed = 0;

                        if (!_graphics.GraphicsDevice.Viewport.Bounds.Contains(tankBounds))
                            base.Update(gameTime);

                    }
                    break;
                case GameState.Menu:
                    {
                        LoadMenuContent();
                    }
                    break;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            // TODO: Add your drawing code here
            _spriteBatch.Begin(SpriteSortMode.BackToFront,
                BlendState.AlphaBlend);


            switch (gameState)
            {
                case GameState.SplashScreen:
                    {
                        DrawSplashScreen();
                    }
                    break;
                case GameState.Running:
                    {
                        DrawGameContent();
                    }
                    break;
                case GameState.Menu:
                    {
                        DrawMenuContent();
                    }
                    break;
            }

            if (showDebug)
            {

                DrawBoundingBox(_spriteBatch);

                //    spriteBatch.DrawString(spFont, string.Format("Speed: {0}", speed.ToString()), new Vector2(50.0f, 50.0f), Color.Yellow);
                //    spriteBatch.DrawString(spFont, string.Format("Rotation Angle: {0}", t34rotationAngle.ToString()), new Vector2(50.0f, 70.0f), Color.Yellow);
                //    spriteBatch.DrawString(spFont, string.Format("World Position: X:{0} Y:{1}", tankPosition.X, tankPosition.Y), new Vector2(50.0f, 90.0f), Color.Yellow);
                //    spriteBatch.DrawString(spFont, string.Format("Viewport Bounds: W:{0} H:{1}", graphics.GraphicsDevice.Viewport.Bounds.Width, graphics.GraphicsDevice.Viewport.Bounds.Height), new Vector2(50.0f, 110.0f), Color.Yellow);
            }

            _spriteBatch.End();

            base.Draw(gameTime);
        }

        private void LoadSplashScreen()
        {
            var ss = Content.Load<Texture2D>(@"graphics\PanzerLehr");
            splashScreen = new GameObject(ss, new Vector2());
            musicEffect = Content.Load<Song>(@"sound\panzerlied_wehrmacht");

        }

        private void LoadGameContent()
        {
            // TODO: use this.Content to load your game content here
            tankTexture = Content.Load<Texture2D>(@"graphics\t34");


            tankPosition.X = _graphics.GraphicsDevice.Viewport.Width / 2;
            tankPosition.Y = _graphics.GraphicsDevice.Viewport.Height / 2;
            origin.X = tankTexture.Width / 2;
            origin.Y = tankTexture.Height / 2;

            //run the color data from the tank texture into an array.
            tankCollisionData = new Color[tankTexture.Width *
                tankTexture.Height];
            tankTexture.GetData(tankCollisionData);

            tankSpeed = Vector2.Zero;




            tankBounds = new Rectangle((int)(tankPosition.X - tankTexture.Width / 2),
            (int)(tankPosition.Y - tankTexture.Height / 2),
            tankTexture.Width, tankTexture.Height);

            //Load the building
            //buildingOrigin.X = 100f;
            //buildingOrigin.Y = 100f;
            buildingOrigin = new Vector2(100f, 100f);
            building = new GameObject(Content.Load<Texture2D>(@"graphics\sprrooftop3"),
              buildingOrigin);


            panzer4 = new GameObject(Content.Load<Texture2D>(@"graphics\panzerIV"),
                 buildingOrigin = new Vector2(600, 300));
            panzerColorData = new Color[panzer4.Texture.Width *
                panzer4.Texture.Height];
            panzer4.Texture.GetData(panzerColorData);

            // Sets the size of the array
            buildingCollisionData = new Color[building.Texture.Width *
                building.Texture.Height];
            building.Texture.GetData(buildingCollisionData);

            grassTexture = Content.Load<Texture2D>(@"graphics\grass");

            spFont = Content.Load<SpriteFont>(@"fonts\Arial");

            tankgunEffect = Content.Load<SoundEffect>(@"sound\tankgun");
            tankgunInstance = tankgunEffect.CreateInstance();

            tankEngineEffect = Content.Load<SoundEffect>(@"sound\tankengine");
            tankengineInstance = tankEngineEffect.CreateInstance();
        }
        private void LoadMenuContent()
        {
            throw new NotImplementedException();
        }

        private void DrawMenuContent()
        {
            throw new NotImplementedException();
        }

        private void DrawGameContent()
        {
            _spriteBatch.Draw(grassTexture, Vector2.Zero,
          null,
          Color.White, 0,
          origin, 2.2f, SpriteEffects.None,
          1f);

            _spriteBatch.Draw(tankTexture, tankPosition,
                null,
                Color.White, t34rotationAngle,
                origin, 1.0f, SpriteEffects.None,
                0f);
            panzer4.Draw(_spriteBatch);
            building.Draw(_spriteBatch);
        }

        private void DrawSplashScreen()
        {
            splashScreen.Draw(_spriteBatch);
        }

        private void DrawBoundingBox(SpriteBatch spriteBatch)
        {
            Texture2D t = new Texture2D(_graphics.GraphicsDevice, 1, 1);
            t.SetData(new[] { Color.White });
            //spriteBatch.Draw(t, tankBounds, Color.Transparent);
            int bw = 2; // Border width
            spriteBatch.Draw(t, new Rectangle(tankBounds.Left, tankBounds.Top, bw, tankBounds.Height), Color.Yellow); // Left
            spriteBatch.Draw(t, new Rectangle(tankBounds.Right, tankBounds.Top, bw, tankBounds.Height), Color.Yellow); // Right
            spriteBatch.Draw(t, new Rectangle(tankBounds.Left, tankBounds.Top, tankBounds.Width, bw), Color.Yellow); // Top
            spriteBatch.Draw(t, new Rectangle(tankBounds.Left, tankBounds.Bottom, tankBounds.Width, bw), Color.Yellow); // Bottom

            spriteBatch.Draw(t, new Rectangle(building.Bounds.Left, building.Bounds.Top, bw, building.Bounds.Height), Color.Red); // Left
            spriteBatch.Draw(t, new Rectangle(building.Bounds.Right, building.Bounds.Top, bw, building.Bounds.Height), Color.Red); // Right
            spriteBatch.Draw(t, new Rectangle(building.Bounds.Left, building.Bounds.Top, building.Bounds.Width, bw), Color.Red); // Top
            spriteBatch.Draw(t, new Rectangle(building.Bounds.Left, building.Bounds.Bottom, building.Bounds.Width, bw), Color.Red); // Bottom
        }

    }
}
﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace PanzerLehr.GameObjects
{
    public class GameObject
    {
        Texture2D texture;
        Vector2 position;
        Rectangle bounds;
        float rotation;

        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        public Vector2 Postion
        {
            get { return position; }
            set { position = value; }
        }

        public Rectangle Bounds
        {
            get { return bounds; }
            set { bounds = value; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public GameObject(Texture2D newTexture, Vector2 newPosition)
        {
            texture = newTexture;
            position = newPosition;

            bounds = new Rectangle((int)position.X, (int)position.Y,
                texture.Width, texture.Height);


            //rectangle = new Rectangle((int)(newPosition.X - newTexture.Width / 2),
            //(int)(newPosition.Y - newTexture.Height / 2),
            //newTexture.Width, newTexture.Height);

        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(texture, bounds, Color.White);
        }

    }
}
